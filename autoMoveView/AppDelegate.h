//
//  AppDelegate.h
//  autoMoveView
//
//  Created by Yeming on 13-9-13.
//  Copyright (c) 2013年 Yeming. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
