//
//  main.m
//  autoMoveView
//
//  Created by Yeming on 13-9-13.
//  Copyright (c) 2013年 Yeming. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
