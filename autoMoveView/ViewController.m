//
//  ViewController.m
//  autoMoveView
//
//  Created by Yeming on 13-9-13.
//  Copyright (c) 2013年 Yeming. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "drawImage.h"
@interface ViewController ()
{
    BOOL _isPress;
}
@property(retain,nonatomic) drawImage *imageVc;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.imageVc=[[drawImage alloc] initWithFrame:CGRectMake(20, 20, 200, 200)];
    self.imageVc.backgroundColor=[UIColor blueColor];
    [self.view addSubview:self.imageVc];
//    UIGraphicsBeginImageContextWithOptions(CGSizeMake(640, 960), YES, 0);
//    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
//    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    CGImageRef imageRef = viewImage.CGImage;
//    CGRect aRect=CGRectMake(0, 0, 100, 100);
//    CGRect rect =aRect;//这里可以设置想要截图的区域
//    CGImageRef imageRefRect =CGImageCreateWithImageInRect(imageRef, rect);
//    UIImage *sendImage = [[UIImage alloc] initWithCGImage:imageRefRect];
//    
//    
//    self.img.image=sendImage;
//    UIImage *image=[UIImage imageNamed:@"erping1.jpg"];
//  //  UIGraphicsBeginImageContext(CGSizeMake(100, 100));
//    CGRect myImageRect = CGRectMake(10.0, 10.0, 57.0, 57.0);
//    CGImageRef ref=image.CGImage;
//    CGImageRef newImage=CGImageCreateWithImageInRect(ref,myImageRect);
//
//    CGContextRef conref=    UIGraphicsGetCurrentContext();
//    CGContextAddArc(conref, 10, 10, 10, 0, 2*M_PI, 0);
//    
//    
//    
//    UIImage *newimage=[[UIImage alloc] initWithCGImage:newImage];
//    self.img.contentMode=1;
//    self.img.image=newimage;
    
    
    
   // NSData *imageViewData = UIImagePNGRepresentation(sendImage);
  //  self.button.selected=NO;
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (_isPress) {
        
    
        UITouch *touch=[touches anyObject];
        
        CGPoint oldPoint=[touch previousLocationInView:touch.view];
        
        CGPoint newPoint=[touch locationInView:touch.view];
        
        CGPoint offSet=CGPointMake(newPoint.x-oldPoint.x, newPoint.y-oldPoint.y);
    self.img.center=CGPointMake(self.img.center.x+offSet.x, self.img.center.y+offSet.y   );
    }
    

}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch=[touches anyObject];
    CGPoint start=[touch locationInView:self.view];
    if (CGRectContainsPoint(self.img.frame, start)) {
        _isPress=YES;
    }
    else
    {
        _isPress=NO;
    }
    
}
@end
