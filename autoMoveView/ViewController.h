//
//  ViewController.h
//  autoMoveView
//
//  Created by Yeming on 13-9-13.
//  Copyright (c) 2013年 Yeming. All rights reserved.
//

#import <UIKit/UIKit.h>

@class drawImage;
@interface ViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet drawImage *imageView;

@end
